<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="StyleSheet" href="estils.css" type="text/css">
		<script type="text/javascript" src="scripts.js"></script>
		
		<title>Aplicació de proves 1</title>
	</head>
	<body>
		<h1>Aplicació de proves 1</h1>
		
		<div id="div_formulariRegistre">
			<h2>Registre</h2>
			<form action="index.php" method="post" id="formulariRegistre">
				<label>Correu electrònic: </label>
				<input type="text" id="formRegistre_email" required />
				<br />
				<label>Contrasenya: </label>
				<input type="password" id="formRegistre_password" />
				<br />
				<label>Nom: </label>
				<input type="text" id="formRegistre_nom" />
				<br />
				<label>Cognoms: </label>
				<input type="text" id="formRegistre_cognoms" />
				<br />
				<label>Edat: </label>
				<select id="formRegistre_select">
					<option value=""> </option>
				<?php
				for ($cont=0; $cont<=130; $cont++) { 
					echo '<option value="'.$cont.'">'.$cont.'</option>';
				}
				?>
				</select>
				<br />
				<label>Administrador?: </label>
				<input type="checkbox" id="formRegistre_admin" />
				<br />
				<br />
				<input type="submit" name="formRegistre_botoEnviar" value="Registrar-se" />
			</form>
		</div>
	</body>
</html>

